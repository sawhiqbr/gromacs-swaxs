#!/usr/bin/env bash

set -e

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

(
  cd "${script_dir}"

  cmake --build build --target install -j $(nproc)
  cmake --build build.mpi --target install -j $(nproc)
)
