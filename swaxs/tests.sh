#!/usr/bin/env bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

(
  cd "${script_dir}"

  if [ ! -e tests ]; then
    git clone git@gitlab.com:cbjh/gromacs-swaxs-tests.git tests
  fi

  source dist/bin/GMXRC.bash

  # For verbose output call with -v switch
  # ./tests/run.t -v
  ./tests/run.t

  # pytest --no-header -v -s
)
