ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ADD packages.yaml.append /home/user/.spack/packages.yaml.append

RUN cat /home/user/.spack/packages.yaml.append >> /home/user/.spack/packages.yaml && rm /home/user/.spack/packages.yaml.append

# CUDA 6.5 is from August 2014 and it is the oldest version available in Spack
# CUDA 7.5 is from September 2015
# For example Gromacs 4.6 was developed between Jan 2013 and Aug 2014

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack install --only dependencies \
        gromacs@2018.8+cuda ^cuda@7.5.18 \
        gromacs@2016.6+cuda ^cuda@7.5.18 \
        gromacs@5.1.5+cuda ^cuda@7.5.18 \
    && spack install --only dependencies gromacs+mpi ^openmpi@3.1.6 \
    && spack install --only dependencies gromacs+mpi ^openmpi@2.1.6 \
    && spack clean --all"

USER root

# The second line of packages is needed for Python Pillow, a dependency of GromacsWrapper
# https://pillow.readthedocs.io/en/stable/installation.html#building-on-linux

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y grace xorg-dev libglu1-mesa libgl1-mesa-dev xvfb libxinerama1 libxcursor1 xmlstarlet bc \
      libtiff5-dev libjpeg8-dev libopenjp2-7-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python3-tk libharfbuzz-dev libfribidi-dev libxcb1-dev \
    && rm -rf /var/lib/apt/lists/*

# sed for ad-hoc editing of configuration

RUN apt-get update \
    && apt-get install -y sed \
    && rm -rf /var/lib/apt/lists/*

USER user

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack install py-pip py-setuptools \
    && spack load py-pip py-setuptools \
    && python -m pip install GromacsWrapper"

RUN git config --global --add safe.directory '*'
