ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ADD packages.yaml.append /home/user/.spack/packages.yaml.append

RUN cat /home/user/.spack/packages.yaml.append >> /home/user/.spack/packages.yaml && rm /home/user/.spack/packages.yaml.append

# CUDA 9.0 is from March 2018, it is the oldest that builds with GCC 6

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack install --only dependencies \
        gromacs@2020.7+cuda ^cuda@9.0.176 \
        gromacs@2019.6+cuda ^cuda@9.0.176 \
        gromacs@2018.8+cuda ^cuda@9.0.176 \
        gromacs@2016.6+cuda ^cuda@9.0.176 \
        gromacs@5.1.5+cuda ^cuda@9.0.176 \
    && spack install --only dependencies gromacs+mpi ^openmpi@3.1.6 \
    && spack install --only dependencies gromacs+mpi ^openmpi@2.1.6 \
    && spack clean --all"

USER root

# https://serverfault.com/a/797318
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y grace xorg-dev libglu1-mesa libgl1-mesa-dev xvfb libxinerama1 libxcursor1 xmlstarlet bc \
    && rm -rf /var/lib/apt/lists/*

# sed for ad-hoc editing of configuration

RUN apt-get update \
    && apt-get install -y sed \
    && rm -rf /var/lib/apt/lists/*

USER user

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack install py-pip py-setuptools \
    && spack load py-pip py-setuptools \
    && python -m pip install GromacsWrapper"

RUN git config --global --add safe.directory '*'
