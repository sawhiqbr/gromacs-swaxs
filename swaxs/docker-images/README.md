# Run

Images are built from GitLab pipelines.

# Development

To build an image you can also run the main Bash script

```shell
./build-image.sh <ubuntu-version> <compiler> <stage>
```

for example

```shell
./build-image.sh 22.04 - base
```

- `<ubuntu-version>` examples: `22.04`, `20.04`, `18.04`
- `<compiler>` accepts Spack notation, examples: `gcc@12.2.0`, `gcc@10.4.0`, `gcc@8.5.0`, `gcc@6.5.0`, `gcc@4.9.4` or `clang@13.0.1`
- `<stage>` can be `base`, `compiler` or `dependencies`

**Note**: _Reopen Folder Locally_, otherwise the script won't be able to access Docker.

# Known issues

Avoid CUDA 11.3 (known GROMACS issue) and 11.5-11.6.1 (doesn't build with GCC 11).
