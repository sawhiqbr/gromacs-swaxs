ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ARG COMPILER

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack spec \${COMPILER/clang/llvm+flang}"

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack install \${COMPILER/clang/llvm+flang} && spack clean --all"

RUN bash -c "source ~/.spack/root/share/spack/setup-env.sh \
    && spack load \${COMPILER/clang/llvm+flang} && spack compiler find"

RUN echo "    compiler: [\"${COMPILER}\"]" >> ~/.spack/packages.yaml
