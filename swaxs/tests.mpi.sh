#!/usr/bin/env bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

# Smaug
# module load gromacs/2018.8

(
  cd "${script_dir}"

  if [ ! -e tests ]; then
    git clone git@gitlab.com:cbjh/gromacs-swaxs-tests.git tests
  fi

  source dist.mpi/bin/GMXRC.bash

  # For verbose output call with -v switch
  ./tests/run.mpi.t -v
  # ./tests/run.mpi.t

  # pytest --no-header -v -s
)
