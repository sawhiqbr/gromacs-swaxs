#!/usr/bin/env bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -e

# # Smaug
# module load gromacs/2018.8
# gmx_gpu="CUDA"

# Docker non-GPU workstation
gmx_gpu="OFF"

(
  cd "${script_dir}/.."

  # It is needed for unpatched GROMACS 2018 replica branch.
  if [[ -e cmake/gmxManageNvccConfig.cmake ]]; then
    # No longer supported by CUDA 12.0
    sed -i -E 's/-gencode;arch=compute_20,code=sm_20;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_20,code=compute_20;?//g'  cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_21,code=sm_21;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_21,code=compute_21;?//g'  cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_30,code=sm_30;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_30,code=compute_30;?//g'  cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_35,code=sm_35;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_35,code=compute_35;?//g'  cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_37,code=sm_37;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_37,code=compute_37;?//g'  cmake/gmxManageNvccConfig.cmake ;

    # Not supported by CUDA 10.2 on Smaug
    sed -i -E 's/-gencode;arch=compute_80,code=sm_80;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_80,code=compute_80;?//g'  cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_86,code=sm_86;?//g'       cmake/gmxManageNvccConfig.cmake ;
    sed -i -E 's/-gencode;arch=compute_86,code=compute_86;?//g'  cmake/gmxManageNvccConfig.cmake ;
  fi
)

(
  cd "${script_dir}"

  rm -rf dist dist.mpi build build.mpi

  # Remove -DGMX_HWLOC="OFF" and -DGMX_SIMD=AVX2_256 after migrating GROMACS 2018 replica branch to GROMACS-SWAXS 2021.

  cmake -DGMX_GPU="${gmx_gpu}" -DGMX_MPI="OFF" -DGMX_HWLOC="OFF" -DGMX_SIMD="AVX2_256" \
      -DCMAKE_INSTALL_PREFIX="$(pwd)/dist" -DCMAKE_C_COMPILER="$(which gcc-10 || which gcc-9 || which gcc-8)" -DCMAKE_CXX_COMPILER="$(which g++-10 || which g++-9 || which g++-8)" -S .. -B build
  rm -f TestCUDA.o
  cmake --build build --target install -j 8

  cmake -DGMX_GPU="${gmx_gpu}" -DGMX_MPI="ON" -DGMX_HWLOC="OFF" -DGMX_SIMD="AVX2_256" \
      -DCMAKE_INSTALL_PREFIX="$(pwd)/dist.mpi" -DCMAKE_C_COMPILER="$(which gcc-10 || which gcc-9 || which gcc-8)" -DCMAKE_CXX_COMPILER="$(which g++-10 || which g++-9 || which g++-8)" -S .. -B build.mpi
  rm -f TestCUDA.o
  cmake --build build.mpi --target install -j 8
)
