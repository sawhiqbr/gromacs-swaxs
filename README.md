# GROMACS-SWAXS

This is a modified GROMACS for SAXS/WAXS/SANS calculations based on explicit solvent models:
- small- and wide-angle X-ray scattering (SAXS/WAXS) calculations
- small-angle neutron scattering (SANS) calculations
- SAXS- or SANS-driven MD simulations, that is, structure refinement against SAXS/SANS data sets

Documentation can be found at
- [cbjh.gitlab.io/gromacs-swaxs-docs](https://cbjh.gitlab.io/gromacs-swaxs-docs)
  - [Installation](https://cbjh.gitlab.io/gromacs-swaxs-docs/installation.html)
  - [Tutorials](https://cbjh.gitlab.io/gromacs-swaxs-docs/tutorials.html)
- [biophys.uni-saarland.de/swaxs.html](https://biophys.uni-saarland.de/swaxs.html)

## Relation to GROMACS

Most of the code in this project comes from GROMACS project which should be recognized in terms of citation and licensing.

To get information about original GROMACS, visit
- https://gitlab.com/gromacs/gromacs/-/blob/main/README
- https://gitlab.com/gromacs/gromacs
- https://manual.gromacs.org
- https://gromacs.bioexcel.eu

GROMACS-SWAXS acts like normal GROMACS, except for
- additional `gmx genenv` and `gmx genscatt` commands
- few extra command line parameters for `gmx mdrun`
- extra `.mdp` options
- additional environmental variables consumed as input

You can find more information in the [Usage](https://cbjh.gitlab.io/gromacs-swaxs-docs/usage.html) section.

## Troubleshooting

When using GROMACS-SWAXS, remember that this is not an official GROMACS and don't report issues to official GROMACS project.  

When encountering problems [create an issue](https://gitlab.com/cbjh/gromacs-swaxs/-/issues) in this project or [contact our team](https://cbjh.gitlab.io/gromacs-swaxs-docs/getting-help.html) directly.
