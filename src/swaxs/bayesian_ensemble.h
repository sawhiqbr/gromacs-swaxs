#pragma once

#include <gromacs/mdtypes/commrec.h>  // t_commrec

#include <waxs/types/waxsrec.h>  // t_waxsrec

namespace swaxs {

/** Init the Bayesian refinement of one state together with the weights of several other fixed states.
 *
 *  Read the SAXS curve of the other states.
 *  See Shevchuk and Hub, Plos Comp Biol, 2017
 */
void init_bayesian_ensemble_refinement(t_waxsrec* wr, t_commrec* cr);

/* Return the maximum-likelihood (ML) estimtate for the fitting parameters f and according to
 *    I(fitted) = f * I(exp) + c
 * These f/c values minimize the residuals between the fitted experimental and the calculated curve.
 *
 * Depending on wr->inputParams->ewaxs_Iexp_fit, this fits either only the scale (f) or both (f and c),
 * or nothing (setting f=1, c=0).
 */
void maximum_likelihood_est_fc_generic(t_waxsrec* wr, double* Icalc, double* Iexp, double* tau, int nq, double* f_ml,
        double* c_ml, double* sumSquaredResiduals);

/** Monte-Carlo Sampling of the weights of the states and of the uncertainty of the buffer density */
void bayesian_gibbs_sampling(t_waxsrec* wr, double simtime, const t_commrec* cr);

void bayesian_ensemble_average(const t_waxsrec* wr, t_waxsrecType* wt, const t_commrec* cr, const waxs_datablock* wd, int nabs);

}
