#pragma once

#include <gromacs/mdrunutility/multisim.h>  // gmx_multisim_t
#include <gromacs/mdtypes/commrec.h>  // t_commrec

#include <waxs/types/waxsrec.h>  // t_waxsrec, t_waxsrecType

namespace swaxs {

void init_maxent_ensemble_refinement(t_waxsrec* wr, const gmx_multisim_t* ms);

void maxent_ensemble_average(t_waxsrec* wr, t_waxsrecType* wt, const t_commrec* cr, const gmx_multisim_t* ms);

}
