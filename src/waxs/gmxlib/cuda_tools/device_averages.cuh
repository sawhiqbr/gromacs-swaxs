#ifndef GMX_WAXS_GMXLIB_CUDA_TOOLS_DEVICE_AVERAGES_H
#define GMX_WAXS_GMXLIB_CUDA_TOOLS_DEVICE_AVERAGES_H

#include <cuda.h>
#include <cuda_runtime.h>

#include <gromacs/math/gmxcomplex.h>  // t_complex
#include <gromacs/utility/real.h>  // real

#include <waxs/gmxcomplex.h>  // t_complex_d

__device__ t_complex_d d_cadd_d(t_complex_d a, t_complex_d b);

__device__ t_complex d_cadd(t_complex a, t_complex b);

__device__ static t_complex d_rcmul(real r, t_complex c);

__device__ t_complex_d d_rcmul_d(double r, t_complex_d c);

__device__ t_complex_d d_cmul_rd(t_complex a, t_complex_d b);

__device__ double d_accum_avg( double sum, double new_value, double fac1, double fac2 );

__device__ t_complex d_c_accum_avg(t_complex sum, t_complex new_value, real fac1, real fac2);

__device__ t_complex_d d_cd_accum_avg(t_complex_d sum, t_complex_d new_value, double fac1, double fac2);

#endif
