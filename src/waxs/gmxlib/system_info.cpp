#include "./system_info.h"

#include <config.h>  // GMX_NATIVE_WINDOWS

#include <thread>
#if GMX_NATIVE_WINDOWS
#  include <windows.h>  // GlobalMemoryStatusEx, MEMORYSTATUSEX
#elif defined(__APPLE__)
#  include <sys/types.h>  // int64_t
#  include <sys/sysctl.h>  // CTL_HW, HW_MEMSIZE, sysctl
#else
#  include <unistd.h>  // _SC_PAGE_SIZE, _SC_PHYS_PAGES, sysconf
#endif

namespace swaxs {

unsigned int SystemInfo::getCpuCoreCount() {
    return static_cast<unsigned int>(std::thread::hardware_concurrency());
}

unsigned long SystemInfo::getTotalMemorySize() {
#if GMX_NATIVE_WINDOWS == 1
    /** Based on https://stackoverflow.com/a/2513561 */
    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx(&status);

    return status.ullTotalPhys;
#elif defined(__APPLE__)
    /** Based on https://stackoverflow.com/a/1909988 */
    int query[2] = { CTL_HW, HW_MEMSIZE };

    int64_t memory;
    size_t length = sizeof(int64_t);
    sysctl(query, 2, &memory, &length, nullptr, 0);

    return memory;
#else
    /** Based on https://stackoverflow.com/a/2513561 */
    return sysconf(_SC_PHYS_PAGES) * sysconf(_SC_PAGE_SIZE);
#endif
}

}
