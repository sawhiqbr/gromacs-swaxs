#include "./tpxio.h"

#include <gromacs/utility/iserializer.h>  // gmx::ISerializer
#include <gromacs/utility/smalloc.h>  // snew
#include <gromacs/mdtypes/inputrec.h>  // t_inputrec

#include <waxs/types/waxstop.h>  // t_scatt_types

void do_waxs_inputrec(gmx::ISerializer* serializer, t_inputrec* ir) {
    serializer->doInt(&ir->waxs_nTypes);
    serializer->doReal(&ir->waxs_tau);
    serializer->doInt(&ir->ewaxs_potential);
    serializer->doInt(&ir->ewaxs_Iexp_fit);
    serializer->doInt(&ir->ewaxs_weights);
    serializer->doInt(&ir->ewaxs_ensemble_type);
    serializer->doReal(&ir->waxs_t_target);
    serializer->doInt(&ir->waxs_nstcalc);
    serializer->doInt(&ir->waxs_nfrsolvent);
    serializer->doInt(&ir->waxs_J);
    serializer->doReal(&ir->waxs_denssolvent);
    serializer->doReal(&ir->waxs_denssolventRelErr);
    serializer->doInt(&ir->ewaxs_correctbuff);
    serializer->doInt(&ir->ewaxs_solvdensUnsertBayesian);
    serializer->doInt(&ir->ewaxs_bScaleI0);
    serializer->doReal(&ir->waxs_warn_lay);
    serializer->doInt(&ir->waxs_nstlog);
    serializer->doReal(&ir->waxs_ensemble_fc);
    serializer->doInt(&ir->waxs_ensemble_nstates);
    serializer->doInt(&ir->waxs_npbcatom);

    if (serializer->reading()) {
        snew(ir->escatter,         ir->waxs_nTypes);
        snew(ir->waxs_fc,          ir->waxs_nTypes);
        snew(ir->waxs_nq,          ir->waxs_nTypes);
        snew(ir->waxs_start_q,     ir->waxs_nTypes);
        snew(ir->waxs_end_q,       ir->waxs_nTypes);
        snew(ir->waxs_deuter_conc, ir->waxs_nTypes);

        snew(ir->waxs_pbcatoms, ir->waxs_npbcatom);
        snew(ir->waxs_ensemble_init_w, ir->waxs_ensemble_nstates);
    }

    serializer->doIntArray(ir->escatter,          ir->waxs_nTypes);
    serializer->doRealArray(ir->waxs_fc,          ir->waxs_nTypes);
    serializer->doIntArray(ir->waxs_nq,           ir->waxs_nTypes);
    serializer->doRealArray(ir->waxs_start_q,     ir->waxs_nTypes);
    serializer->doRealArray(ir->waxs_deuter_conc, ir->waxs_nTypes);
    serializer->doRealArray(ir->waxs_end_q,       ir->waxs_nTypes);

    if (ir->waxs_npbcatom > 0) {
        serializer->doIntArray(ir->waxs_pbcatoms, ir->waxs_npbcatom);
    }

    if (ir->waxs_ensemble_nstates > 0) {
        if (serializer->reading()) {
            snew(ir->waxs_ensemble_init_w, ir->waxs_ensemble_nstates);
        }

        serializer->doRealArray(ir->waxs_ensemble_init_w, ir->waxs_ensemble_nstates);
    }
}

void do_scattering_types(gmx::ISerializer* serializer, t_scatt_types* scattTypes) {
    if (serializer->reading()) {
        scattTypes->nCromerMannParameters = 0;
        scattTypes->nNeutronScatteringLengths = 0;

        scattTypes->cromerMannParameters = nullptr;
        scattTypes->neutronScatteringLengths = nullptr;
    }

    serializer->doInt(&scattTypes->nCromerMannParameters);
    serializer->doInt(&scattTypes->nNeutronScatteringLengths);

    int ncm = scattTypes->nCromerMannParameters;
    int nnsl = scattTypes->nNeutronScatteringLengths;

    if (serializer->reading()) {
        snew(scattTypes->cromerMannParameters, ncm);
        snew(scattTypes->neutronScatteringLengths, nnsl);
    }

    for (int i = 0; i < ncm; i++) {
        serializer->doReal(&scattTypes->cromerMannParameters[i].a[0]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].a[1]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].a[2]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].a[3]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].b[0]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].b[1]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].b[2]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].b[3]);
        serializer->doReal(&scattTypes->cromerMannParameters[i].c);
    }

    for (int i = 0; i < nnsl; i++) {
        serializer->doReal(&scattTypes->neutronScatteringLengths[i].cohb);
    }
}
