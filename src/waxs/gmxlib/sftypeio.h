#pragma once

#include <waxs/types/waxsrec.h>  // t_waxsrec
#include <waxs/types/waxstop.h>  // t_scatt_types, t_neutron_sl

/* Note: For water, we use corrected atomic form factors, see the note in cromer-mann-defs.itp.

   We use the correction suggested by Daniel Bowron in his EPSR tutorial (March 29,2010)
   Also see Sorensen et al, J. Chem. Phys. 113,9149 (2000)
   Note: We use NOT the Sorensen et al, formula, but instead
         f'(Q) = [ 1 + alpha*exp(-Q^2/2delta^2) ] * f(Q)

  with alpha_O = 0.12
       alpha_H = -0.48
       delta   = 22
*/

/* Placeholders for deuteratable hydrogen atoms. These MUST correspond to
 * definitions in share/top/neutron-scatt-len-defs.itp
 */
#define NSL_H_DEUTERATABLE            -1000
#define NSL_H_DEUTERATABLE_BACKBONE   -2000

#define NEUTRON_SCATT_LEN_1H        (-3.7406)
#define NEUTRON_SCATT_LEN_2H          6.671
#define NEUTRON_SCATT_LEN_O           5.803

#define CROMERMANN_2_NELEC(x) (x.c + x.a[0] + x.a[1] + x.a[2] + x.a[3])

/** Init and free scattering types (Cromer-Mann or NSL types) */
void init_scattering_types(t_scatt_types* t);
void done_scattering_types(t_scatt_types* t);

/** Print the 9 Cromer-Mann parameters */
void print_cmsf(FILE* out, t_cromer_mann* cm, int nTab);

/** Return the index of existing Cromer-Mann or NSL type if found and NOTSET if not found. */
int search_scattTypes_by_cm(const t_scatt_types* scattTypes, t_cromer_mann* cm_new);
int search_scattTypes_by_nsl(const t_scatt_types* scattTypes, t_neutron_sl* nsl_new);

/* Generic adder for sorted-unque list of scattering types (Cromer-Mann or NSL types). */
int add_scatteringType(t_scatt_types* sim_sf, t_cromer_mann* cmsf, t_neutron_sl* nusf);

/** Return the Neutron Scattering Length (NSL) type, taking the D2O concentation of this waxs-type t into account */
int get_nsl_type(t_waxsrec* wr, int t, int nslTypeIn);

/** Assert that the scattering type of this atom number has been set */
void assert_scattering_type_is_set(int stype, int type, int atomno);

/** Prepare the atomic form factor tables (aff_table), one for each Cromer-Mann type */
void compute_atomic_form_factor_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, t_commrec* cr);

/** Make a table of NSLs found in the topology, and add NSLs of 1H and 2H */
void fill_neutron_scatt_len_table(const t_scatt_types* scattTypes, t_waxsrecType* wt, real backboneDeuterProb);

/** Atomic form factor f(q) from given Cromer-Mann parameters */
double CMSF_q(t_cromer_mann cmsf, real q);
