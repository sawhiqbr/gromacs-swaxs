#pragma once

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t
#include <gromacs/pbcutil/pbc.h>  // PbcType
#include <gromacs/topology/topology.h>  // gmx_mtop_t

#include <waxs/envelope/envelope.h>  // gmx_envelope

namespace swaxs {

class Solvent {
public:
    Solvent() = default;

    void readTrajectory(const char* trajectoryFilePath, const gmx_output_env_t* oenv, const int frameLimit);

    Solvent(const char* runFilePath, const char* trajectoryFilePath, const gmx_output_env_t* oenv,
            const gmx_bool bVerbose, const int frameLimit);

    ~Solvent() {
        sfree(box);

        for (int i = 0; i < nFrames; i++) {
            sfree(x[i]);
        }

        sfree(x);
    }

    /** Reset frame id of pure-solvent frames, so start using form the first frame */
    void resetPureSolventFrameCounter() {
        this->iFrame = 0;
    }

    /** Neutron scattering lengths found in solvent .tpr file */
    gmx_bool hasNeutron() const {
        return this->topology->scattTypes.nNeutronScatteringLengths != 0;
    }

    /**
     * Move the pure-solvent frame onto the envelope, so we can use the pure-solvent frame
     * to get the excluded solvent.
     *
     * Preparing a frame 'ws->xPrepared' of the pure-solvent simulation:
     *   1. If the SWAXS step is larger than the number of frames: shift the water system so we get an independent
     *      water configuration inside the envelope.
     *   2. Move waterbox to the center of the envelope.
     *   3. Assert that the envelope fully fits into the box.
     *
     * On exit, ws->xPrepared is on the envelope.
     */
    void prepareNextFrame(gmx_envelope* envelope, int verbosityLevel);

    /**
     * Get and distribute density of the pure solvent box.
     * We use the density compute inside the envelope to specify the density correction. This way,
     * the density entering for B(q) is (hopefully) *exactly* the value given by waxs-solvdens.
     */
    void getDensity(int nindB_sol, int* indB_sol, uint32_t* shiftsInsideEnvelope_B, gmx_envelope* envelope,
            gmx_bool bFixSolventDensity, real givenElecSolventDensity, int verbosityLevel);

    std::unique_ptr<gmx_mtop_t> topology;
    PbcType pbcType = PbcType::Unset;

    int* cromerMannTypes = nullptr;  /**< Cromer-Mann types, array of size topology->natoms */
    int* neutronScatteringLengthTypes = nullptr;  /**< NSL types, array of size topology->natoms */

    int nFrames = 0;  /**< Number of frames in **x */
    rvec** x = nullptr;  /**< Coordinates of water frames */
    matrix* box = nullptr;  /**< Boxes of water frames */

    rvec* xPrepared = nullptr;  /**< Coordinates shifted onto the envelope */
    matrix boxPrepared;  /**< Box of prepared frame */

    real averageInverseVolume = 0.;  /**< Average inverse volume */
    real averageDensity = 0.;  /**< Average density, computed from complete box (e/nm^3) */
    real averageDropletDensity = 0.;  /**< Average density of droplet (hopefully the same as average density) */

    double* nElectronsPerAtom = nullptr;  /**< Number of electrons per atom, size topology->natoms */
    real nElectrons = 0.;  /**< Total number of electrons (from Cromer-Mann parameters) */

    double* averageScatteringLengthDensity = nullptr;  /**< Average scattering length density
            (in electrons or NSLs per nm3) for each scattering type (SAXS and SANS with different D2O) */

private:
    int iFrame = 0;  /**< Frame number used in this SWAXS step */
};

};
