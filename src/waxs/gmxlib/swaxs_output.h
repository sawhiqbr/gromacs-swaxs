#pragma once

#include <cstdio>  // FILE

#include <gromacs/fileio/oenv.h>  // gmx_output_env_t

#include <waxs/gmxlib/input_params.h>  // swaxs::InputParams
#include <waxs/gmxlib/swaxs_datablock.h>  // waxs_datablock
#include <waxs/types/waxsrec.h>  // t_waxsrec

struct t_waxsrec;

namespace swaxs {

class SwaxsOutput {
public:
    SwaxsOutput() = default;

    SwaxsOutput(t_waxsrec* wr, const char* fnOut, const gmx_output_env_t* oenv);

    /** Write final WAXS output */
    void writeOut(t_waxsrec* wr, const gmx_output_env_t* oenv);

    ~SwaxsOutput() = default;

    /** Output that needs only one file, even with multiple scattering types */
    FILE* fpLog;
    char* fnLog, * fnDensity;

    /** Output that need an extra file for each scattering type (xray, neutron, etc) */
    FILE** fpSpectra, ** fpStddevs,                           ** fpForces, ** fpSpecEns, ** fpPot, ** fpNindep, ** fpD;
    char** fnSpectra, ** fnStddevs, ** fnFinal, ** fnContrib, ** fnForces, ** fnSpecEns, ** fnPot;
    FILE** fpAvgA, ** fpAvgB, ** fpGibbsEnsW, ** fpGibbsSolvDensRelErr;
    char** fnAvgA, ** fnAvgB, ** fnGibbsEnsW, ** fnGibbsSolvDensRelErr, ** fnNindep, ** fnD;

    t_fileio **xfout;
};

}

/* Write a SWAXS curve to a file */
void write_sas_intensity(FILE* fp, t_waxsrec* wr, int type);

/** Write the errors of the SAS curve (calculated, experiental, and error from 
 * uncertainty of buffer density
 */
void write_sas_curve_errors(t_waxsrec* wr, int64_t step, int t);

/** Write total force and torque from SAS-restraints to the log file */
void write_total_force_torque(t_waxsrec* wr, gmx::ArrayRef<gmx::RVec> x);



/* **************************************************************************************************
 * The following functions were originally written to collect ALL the current averages, with the aim
 * to enable writing and reading of a checkpoint file. However, checkpointing does not work anyway,
 * and it is not so important either. Therefore, these functions should be replaced by something
 * much simpler that collects only the A(q), B(q), and D(q), as these can be written to files.
 ****************************************************************************************************/

/* Does MPI communication between global averages on master, and its local copies,
 * using a prepared nq_masterlist, qoff_masterlist on master to determine
 * size and offsets of arrays based on q-points handled by the node.
 * Combines dissemination and collection below into one function.
 * Without MPI, switches to an old inefficient method.
 * bCollect true is inwards communication, false is outwards communication */
void swaxsProcessGlobalAvs(t_waxsrec* wr, const t_commrec* cr, gmx_bool bCollect);

/* Checks if global are present, and if not, allocate them. */
void swaxsAllocateGlobalAvs(t_waxsrec* wr);

/* Checks if global are present, and if so, free them. */
void swaxsFreeGlobalAvs(t_waxsrec* wr);
