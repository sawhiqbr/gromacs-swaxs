/**
 * This is a subset of random.cpp present in Gromacs 5.1.
 * Copyright belongs to authors of Gromacs.
 *
 * This partial copy is a stop-gap solution. New API should be used in gmx_envelope.cpp.
 */

#include "./random.h"

#include "config.h"

#include <stdio.h>
#include <stdlib.h>  // malloc

#include "gromacs/utility/sysinfo.h"

#define RNG_N 624
#define RNG_M 397
#define RNG_MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define RNG_UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define RNG_LOWER_MASK 0x7fffffffUL /* least significant r bits */

struct gmx_rng {
    unsigned int  mt[RNG_N];
    int           mti;
};

gmx_rng_t
gmx_rng_init(unsigned int seed)
{
    struct gmx_rng *rng;

    if ((rng = (struct gmx_rng *)malloc(sizeof(struct gmx_rng))) == NULL)
    {
        return NULL;
    }

    rng->mt[0] = seed & 0xffffffffUL;
    for (rng->mti = 1; rng->mti < RNG_N; rng->mti++)
    {
        rng->mt[rng->mti] =
                (1812433253UL * (rng->mt[rng->mti-1] ^
                                 (rng->mt[rng->mti-1] >> 30)) + rng->mti);
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        rng->mt[rng->mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
    return rng;
}

gmx_rng_t
gmx_rng_init_array(unsigned int seed[], int seed_length)
{
    int       i, j, k;
    gmx_rng_t rng;

    if ((rng = gmx_rng_init(19650218UL)) == NULL)
    {
        return NULL;
    }

    i = 1; j = 0;
    k = (RNG_N > seed_length ? RNG_N : seed_length);
    for (; k; k--)
    {
        rng->mt[i] = (rng->mt[i] ^ ((rng->mt[i-1] ^
                                     (rng->mt[i-1] >> 30)) * 1664525UL))
                     + seed[j] + j;          /* non linear */
        rng->mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++; j++;
        if (i >= RNG_N)
        {
            rng->mt[0] = rng->mt[RNG_N-1]; i = 1;
        }
        if (j >= seed_length)
        {
            j = 0;
        }
    }
    for (k = RNG_N-1; k; k--)
    {
        rng->mt[i] = (rng->mt[i] ^ ((rng->mt[i-1] ^
                                     (rng->mt[i-1] >> 30)) *
                                    1566083941UL))
                     - i;                    /* non linear */
        rng->mt[i] &= 0xffffffffUL; /* for WORDSIZE > 32 machines */
        i++;
        if (i >= RNG_N)
        {
            rng->mt[0] = rng->mt[RNG_N-1]; i = 1;
        }
    }

    rng->mt[0] = 0x80000000UL;
    /* MSB is 1; assuring non-zero initial array */
    return rng;
}

unsigned int
gmx_rng_make_seed(void)
{
    FILE              *fp;
    unsigned int       data;
    long               my_pid;

#ifdef HAVE_UNISTD_H
    /* We never want Gromacs execution to halt 10-20 seconds while
     * waiting for enough entropy in the random number generator.
     * For this reason we should NOT use /dev/random, which will
     * block in cases like that. That will cause all sorts of
     * Gromacs programs to block ~20 seconds while waiting for a
     * super-random-number to generate cool quotes. Apart from the
     * minor irritation, it is really bad behavior of a program
     * to abuse the system random numbers like that - other programs
     * need them too.
     * For this reason, we ONLY try to get random numbers from
     * the pseudo-random stream /dev/urandom, and use other means
     * if it is not present (in this case fopen() returns NULL).
     */
    fp = fopen("/dev/urandom", "rb");
#else
    fp = NULL;
#endif
    if (fp != NULL)
    {
        fread(&data, sizeof(unsigned int), 1, fp);
        fclose(fp);
    }
    else
    {
        /* No random device available, use time-of-day and process id */
        my_pid = gmx_getpid();
        data   = (unsigned int)(((long)time(NULL)+my_pid) % (long)1000000);
    }
    return data;
}


/* The random number state contains RNG_N entries that are returned one by
 * one as random numbers. When we run out of them, this routine is called to
 * regenerate RNG_N new entries.
 */
static void
gmx_rng_update(gmx_rng_t rng)
{
    unsigned int       x1, x2, y, *mt;
    int                k;
    const unsigned int mag01[2] = {0x0UL, RNG_MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    /* update random numbers */
    mt  = rng->mt;  /* pointer to array - avoid repeated dereferencing */

    x1        = mt[0];
    for (k = 0; k < RNG_N-RNG_M-3; k += 4)
    {
        x2      = mt[k+1];
        y       = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
        mt[k]   = mt[k+RNG_M]   ^ (y >> 1) ^ mag01[y & 0x1UL];
        x1      = mt[k+2];
        y       = (x2 & RNG_UPPER_MASK) | (x1 & RNG_LOWER_MASK);
        mt[k+1] = mt[k+RNG_M+1] ^ (y >> 1) ^ mag01[y & 0x1UL];
        x2      = mt[k+3];
        y       = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
        mt[k+2] = mt[k+RNG_M+2] ^ (y >> 1) ^ mag01[y & 0x1UL];
        x1      = mt[k+4];
        y       = (x2 & RNG_UPPER_MASK) | (x1 & RNG_LOWER_MASK);
        mt[k+3] = mt[k+RNG_M+3] ^ (y >> 1) ^ mag01[y & 0x1UL];
    }
    x2        = mt[k+1];
    y         = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
    mt[k]     = mt[k+RNG_M] ^ (y >> 1) ^ mag01[y & 0x1UL];
    k++;
    x1        = mt[k+1];
    y         = (x2 & RNG_UPPER_MASK) | (x1 & RNG_LOWER_MASK);
    mt[k]     = mt[k+RNG_M] ^ (y >> 1) ^ mag01[y & 0x1UL];
    k++;
    x2        = mt[k+1];
    y         = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
    mt[k]     = mt[k+RNG_M] ^ (y >> 1) ^ mag01[y & 0x1UL];
    k++;
    for (; k < RNG_N-1; k += 4)
    {
        x1      = mt[k+1];
        y       = (x2 & RNG_UPPER_MASK) | (x1 & RNG_LOWER_MASK);
        mt[k]   = mt[k+(RNG_M-RNG_N)]   ^ (y >> 1) ^ mag01[y & 0x1UL];
        x2      = mt[k+2];
        y       = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
        mt[k+1] = mt[k+(RNG_M-RNG_N)+1] ^ (y >> 1) ^ mag01[y & 0x1UL];
        x1      = mt[k+3];
        y       = (x2 & RNG_UPPER_MASK) | (x1 & RNG_LOWER_MASK);
        mt[k+2] = mt[k+(RNG_M-RNG_N)+2] ^ (y >> 1) ^ mag01[y & 0x1UL];
        x2      = mt[k+4];
        y       = (x1 & RNG_UPPER_MASK) | (x2 & RNG_LOWER_MASK);
        mt[k+3] = mt[k+(RNG_M-RNG_N)+3] ^ (y >> 1) ^ mag01[y & 0x1UL];
    }
    y           = (x2 & RNG_UPPER_MASK) | (mt[0] & RNG_LOWER_MASK);
    mt[RNG_N-1] = mt[RNG_M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

    rng->mti = 0;
}

/* Return a random unsigned integer, i.e. 0..4294967295
 * Provided in header file for performace reasons.
 * Unfortunately this function cannot be inlined, since
 * it needs to refer the internal-linkage gmx_rng_update().
 */
unsigned int
gmx_rng_uniform_uint32(gmx_rng_t rng)
{
    unsigned int y;

    if (rng->mti == RNG_N)
    {
        gmx_rng_update(rng);
    }
    y = rng->mt[rng->mti++];

    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

/* Return a uniform floating point number on the interval 0<=x<1 */
real
gmx_rng_uniform_real(gmx_rng_t rng)
{
    if (sizeof(real) == sizeof(double))
    {
        return ((double)gmx_rng_uniform_uint32(rng))*(1.0/4294967296.0);
    }
    else
    {
        return ((float)gmx_rng_uniform_uint32(rng))*(1.0/4294967423.0);
    }
    /* divided by the smallest number that will generate a
     * single precision real number on 0<=x<1.
     * This needs to be slightly larger than MAX_UNIT since
     * we are limited to an accuracy of 1e-7.
     */
}
