#include "./bounding_sphere.h"

#include <gromacs/math/vec.h>  // as_rvec_array, copy_rvec, gmx::RVec, norm2, rvec, rvec_add, rvec_sub, real
#include <gromacs/utility/arrayref.h>  // gmx::ArrayRef
#include <gromacs/utility/fatalerror.h>  // gmx_fatal, FARGS
#include <gromacs/utility/smalloc.h>  // snew, sfree

#include <waxs/gmxlib/gmx_miniball.h>  // gmx_miniball_destroy, gmx_miniball_squared_radius, gmx_miniball_t, NT
#include <waxs/gmxlib/waxsmd_utils.h>  // get_cog

/** Get the atom with the largest distance from a, store index in imax, and return this largest distance */
static real largest_dist_atom(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec a, int* imax) {
    real d2max = 0;
    rvec v;

    *imax = 0;

    for (int i = 0; i < n; i++) {
        rvec_sub(x[index[i]], a, v);
        real d2 = norm2(v);

        if (d2 > d2max) {
            d2max = d2;
            *imax = i;
        }
    }

    return sqrt(d2max);
}

/** Draw a sphere that includes the sphere around cent with radius R as well as point x. Upate cent and R. */
static void update_bounding_sphere(rvec cent, real* R, rvec x) {
    rvec v;
    rvec_sub(cent, x, v);
    real l = sqrt(norm2(v));
    svmul(1. / l, v, v);

    /* update R and center */
    *R = (l + *R) / 2;
    svmul(*R, v, v);
    rvec_add(x, v, cent);
}

#if 0
static void get_bounding_sphere_COM(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent, real* R, gmx_bool bVerbose) {
    if (n == 0) {
        gmx_fatal(FARGS, "Error while getting bounding sphere: zero atoms in index\n");
    }

    if (n == 1) {
        copy_rvec(x[index[0]], cent);
        *R = 0;

        return;
    }

    rvec xcom;
    clear_rvec(xcom);

    /** Need COM of protein for both versions */
    get_cog(x, index, n, xcom);

    /** Get largest distance from COM */
    int imax1 = 0;
    rvec v1;
    largest_dist_atom(x, index, n, xcom, &imax1);
    rvec_sub(x[index[imax1]], xcom, v1);
    *R = sqrt(norm2(v1));
    copy_rvec(xcom, cent);

    if (bVerbose) {
        fprintf(stderr, "COM-method: %g from [%g, %g, %g]\n", *R, xcom[0], xcom[1], xcom[2]);
    }
}
#endif

void get_bounding_sphere_Ritter_COM(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent, real* R, gmx_bool bVerbose) {
    const real eps = 1e-6;

    static gmx_bool bDoRitter = TRUE;
    static gmx_bool bDoCOM = TRUE;
    static gmx_bool bFirst = TRUE;

    static int ritterAtom1 = -1;
    static int ritterAtom2 = -1;

    if (n == 0) {
        gmx_fatal(FARGS, "Error while getting bounding sphere: zero atoms in index\n");
    }

    if (n == 1) {
        copy_rvec(x[index[0]], cent);
        *R = 0;

        return;
    }

    /**
     * On the first call of this function, we get the bounding sphere around the COM AND
     * from Ritter's algorithm. Then we take the smaller sphere, and stick to this method
     * for the rest of the simulation.
     */
    real Rcom = 1e20;
    real Rritter = 1e20;

    rvec xcom;
    rvec centRitter;
    clear_rvec(xcom);
    clear_rvec(centRitter);

    /** Need COM of protein for both versions */
    get_cog(x, index, n, xcom);

    int imax1 = 0;
    int imax2 = 0;

    if (bDoCOM) {
        /** Get largest distance from COM */
        largest_dist_atom(x, index, n, xcom, &imax1);
        rvec v1;
        rvec_sub(x[index[imax1]], xcom, v1);
        Rcom = sqrt(norm2(v1));

        if (bVerbose) {
            fprintf(stderr, "COM-method: %g from [%g, %g, %g]\n", Rcom, xcom[0], xcom[1], xcom[2]);
        }
    }

    int nit = 0;

    if (bDoRitter) {
        /** Ritter: */
        if (ritterAtom1 == -1) {
            /**
             * When this function is called the first time, we choose the two
             * initial atoms for Ritter's algorithm. In the next calls, we start
             * with the same two atoms. We hope that this will make the Ritter
             * bounding sphere more stable
             */

            /** Point X with largest distance from COM */
            largest_dist_atom(x, index, n, xcom, &imax1);

            /** Point Y with largest distance from X */
            largest_dist_atom(x, index, n, x[index[imax1]], &imax2);

            ritterAtom1 = index[imax1];
            ritterAtom2 = index[imax2];
        }

        /** Sphere around center of X-Y line */
        rvec v1;
        rvec_add(x[ritterAtom1], x[ritterAtom2], v1);
        svmul(0.5, v1, centRitter);
        rvec_sub(x[ritterAtom1], x[ritterAtom2], v1);
        Rritter = sqrt(norm2(v1)) / 2;

        /**
         * Ritter's algorithm would here do a loop over all atoms, and expand the sphere each time the atom
         * is outside. Here, we simply pick the atom with the largest distance and expand the sphere accordingly
         */
        real d;

        while ((d = largest_dist_atom(x, index, n, centRitter, &imax1)) > (Rritter + eps)) {
            // printf("Update bounding sphere imax %d, d %g (R = %g)\n", imax, d, *R);
            update_bounding_sphere(centRitter, &Rritter, x[index[imax1]]);

            if (nit++ > 10000) {
                gmx_fatal(FARGS, "Error while getting bounding sphere: No convergence after %d iterations\n", nit);
            }
        }

        if (bVerbose) {
            fprintf(stderr, "Ritter-sph: %g from [%g, %g, %g]\n", Rritter, centRitter[0], centRitter[1], centRitter[2]);
        }
    }

    if (Rritter < Rcom) {
        *R = Rritter;
        copy_rvec(centRitter, cent);
        bDoCOM = FALSE;
    } else {
        *R = Rcom;
        copy_rvec(xcom, cent);
        bDoRitter = FALSE;
    }

    if (bFirst) {
        // printf("\tContructed bounding sphere based on %s.\n", bDoRitter ? "Ritter's algorithm" : "center-of-mass");
        printf("\tR(Ritter)         = %g\n\tR(center-of-mass) = %g\n", Rritter, Rcom);
        bFirst = FALSE;
    }

    if (bVerbose and bFirst) {
        FILE* fp = fopen("bounding_sphere.py", "w");

        fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
        fprintf(fp, "obj = [\n\nALPHA,  0.4,\n");
        fprintf(fp, "COLOR, %g, %g, %g,\n\n", 0.1, 0.8, 0.4);

        /** Draw bounding sphere around origin, since the protein will be shifted there */
        fprintf(fp, "SPHERE, %g,%g,%g, %g,\n", 10 * cent[0], 10 * cent[1], 10 * cent[2], 10 * (*R));
        fprintf(fp, "]\n\ncmd.load_cgo(obj, 'boundsphere')\n\n");
        fclose(fp);

        printf("\nWrote bounding sphere to pymol CGO file %s\n\n", "bounding_sphere.py");
    }
}

void get_bounding_sphere(gmx::ArrayRef<gmx::RVec> x, int* index, int n, rvec cent, real* R, gmx_bool bVerbose) {
    static gmx_bool bFirst = TRUE;

    /* NT is the floating point variable in gmx_miniball.c. It may be either real or double */

    if (n == 0) {
        gmx_fatal(FARGS, "Error while getting bounding sphere: zero atoms in index\n");
    }

    if (n == 1) {
        copy_rvec(x[index[0]], cent);
        *R = 0;

        return;
    }

    NT relerr;
    NT subopt;
    NT* xNT = nullptr;

    if (sizeof(NT) != sizeof(real)) {
        snew(xNT, DIM * n);

        for (int i = 0; i < n; i++) {
            for (int d = 0; d < DIM; d++) {
                /** Copy real array x into linear NT array xNT */
                xNT[DIM * i + d] = x[index[i]][d];
            }
        }

        // gmx_fatal(FARGS, "You are trying to run the Miniball code in a different precision than Gromacs\n"
        //         "This is possible, but then you need to copy the solute atom coords to an array of the"
        //         "NT precision\n");
    }

    /** Need a linear NT* array with pointers to the solute atoms */
    NT** xsoluteNT;
    snew(xsoluteNT, n);

    auto xRaw = as_rvec_array(x.data());

    for (int i = 0; i < n; i++) {
        if (xNT == nullptr) {
            /** Make pointers on the x array - used if NT is type real - do explicit cast to avoid warning if
                NT is double and x is real (so if this line is not used anyway) */
            xsoluteNT[i] = (NT*)xRaw[index[i]];
        } else {
            /** Make pointers on the xNT array - used if NT is not of type real real */
            xsoluteNT[i] = xNT + DIM * i;
        }
    }

    gmx_miniball_t mb = gmx_miniball_init(DIM, xsoluteNT, n);

    if (!gmx_miniball_is_valid(mb, -1)) {
        relerr = gmx_miniball_relative_error(mb, &subopt);

        if (relerr < 1e-3 and subopt < 0.1) {
            fprintf(stderr, "\nWARNING, Miniball did not converged to the default of 10 x machine precision\n"
                    "\tbut to %f. This is not a problem (but worth mentioning).\n\n", relerr);
        } else if (relerr < 1e-3) {
            fprintf(stderr, "\nWARNING, Miniball generated a slighly sub-optimal bounding sphere (%f instead of 0).\n"
                    "\tThis is not a problem (but worth mentioning).\n\n", subopt);
        } else {
            fflush(stdout);
            fprintf(stderr, "\n\nThe generated Miniball is invalid.\n");
            FILE* fp = fopen("miniball_coords_error.dat", "w");

            for (int i = 0; i < n; i++) {
                fprintf(fp, "%.15f %.15f %.15f\n", xsoluteNT[i][XX], xsoluteNT[i][YY], xsoluteNT[i][ZZ]);
            }

            fclose(fp);
            fprintf(stderr, "Dumped %d coordinates that made an invalid Miniball to miniball_coords_error.dat\n", n);
            fprintf(stderr, "Relative error = %g, suboptimality = %g\n", relerr, subopt);

            gmx_fatal(FARGS, "Generating the bounding sphere with Miniball failed.\n");
        }
    }

    double miniball_time = gmx_miniball_get_time(mb);

    NT* centNT = gmx_miniball_center(mb);

    for (int d = 0; d < DIM; d++) {
        cent[d] = centNT[d];
    }

    *R = sqrt(gmx_miniball_squared_radius(mb));

    sfree(xsoluteNT);

    if (xNT) {
        sfree(xNT);
    }

    gmx_miniball_destroy(&mb);

    if (bFirst) {
        printf("Contructed bounding sphere using Miniball.\n");
        printf("\tR(Miniball) = %g\n\tcenter = %g %g %g\n", *R, cent[XX], cent[YY], cent[ZZ]);

        /** Do for comparison also Ritter / COM */
        real Rrittercom;
        rvec centtmp;
        get_bounding_sphere_Ritter_COM(x, index, n, centtmp, &Rrittercom, FALSE);
        printf("For comparison: Ritter/COM gives R = %g, which is %.2f %% larger than Miniball)\n", Rrittercom,
                (Rrittercom - *R) / (*R) * 100);

        bFirst = FALSE;
        printf("Construction of bounding sphere took %g sec.\n", miniball_time);
    }

    if (bVerbose or bFirst) {
        FILE* fp = fopen("bounding_sphere.py", "w");
        fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n\n");
        fprintf(fp, "obj = [\n\nALPHA,  0.4,\n");
        fprintf(fp, "COLOR, %g, %g, %g,\n\n", 0.1, 0.8, 0.4);
        /* Draw bounding sphere around origin, since the protein will be shifted there */
        fprintf(fp, "SPHERE, %g,%g,%g, %g,\n", 10 * cent[0], 10 * cent[1], 10 * cent[2], 10 * (*R));
        fprintf(fp, "]\n\ncmd.load_cgo(obj, 'boundsphere')\n\n");
        fclose(fp);
        printf("\nWrote bounding sphere to pymol CGO file %s\n\n", "bounding_sphere.py");
    }
}
