#pragma once

#include <gromacs/mdtypes/inputrec.h>  // t_inputrec
#include <gromacs/utility/basedefinitions.h>  // gmx_bool

#include <waxs/types/waxstop.h>  // t_scatt_types

void pr_inputrec_waxs(FILE* fp, int indent, const t_inputrec* ir, gmx_bool bMDPformat);

void pr_scattTypes(FILE* fp, int indent, const char* title, const t_scatt_types* scattTypes, gmx_bool bShowNumbers);
