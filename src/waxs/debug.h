#pragma once

#include <cstdio>  // fprintf, stderr

static inline void _swaxs_debug(const char* str, const char* file, int linenr) {
    fprintf(stderr, "DEGUB: %s, line %4d -- %s\n", file, linenr, str ? str : "");
}

/** Change 0 to 1, to enable verbose output from SWAXS code. */
#if 0
    #define swaxs_debug(x) _swaxs_debug(x, __FILE__, __LINE__)
#else
    #define swaxs_debug(x)
#endif
