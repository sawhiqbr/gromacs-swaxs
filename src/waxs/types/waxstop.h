#pragma once

#include <gromacs/utility/real.h>  // real

typedef struct {
    real a[4];
    real b[4];
    real c;
} t_cromer_mann;

/* Neutron scattering length (could later add incoh. b, ) */
typedef struct {
    real cohb;
} t_neutron_sl;

/* The data type written into the tpr and used in simulation. Minimised. */
typedef struct {
    int nCromerMannParameters;
    int nNeutronScatteringLengths;

    t_cromer_mann* cromerMannParameters;
    t_neutron_sl* neutronScatteringLengths;
} t_scatt_types;
