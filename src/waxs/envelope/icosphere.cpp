#include "./icosphere.h"

#include <gromacs/math/vec.h>  // dcprod, diprod, dnorm2, dsvmul, dvec_add, dvec_sub
#include <gromacs/utility/fatalerror.h>  // FARGS, gmx_fatal
#include <gromacs/utility/futil.h>  // gmx_ffopen, gmx_ffclose
#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree

#if defined( ENVELOPE_DEBUG )
#define DEBUG_PRINTF(x) printf x
#else
#define DEBUG_PRINTF(x)
#endif

static int icosphere_add_vertex(t_icosphere* ico, double x, double y, double z) {
    double len;

    len = sqrt(x * x + y * y + z * z);
    srenew(ico->vertex, ico->nvertex + 1);

    ico->vertex[ico->nvertex][XX] = x / len;
    ico->vertex[ico->nvertex][YY] = y / len;
    ico->vertex[ico->nvertex][ZZ] = z / len;

    ico->nvertex++;

    return ico->nvertex - 1;
}

static void icosphere_add_face(t_icosphere* ico, int irec, int a, int b, int c) {
    /* Add one face to recusion level irec */
    ico->rec_nface[irec] += 1;
    srenew(ico->rec_face[irec], ico->rec_nface[irec]);

    t_icosphere_face* thisface;

    thisface = &(ico->rec_face[irec][ico->rec_nface[irec] - 1]);
    thisface->v[0] = a;
    thisface->v[1] = b;
    thisface->v[2] = c;

    dvec tmp, v1, v2;

    /* Store normal. Important: We cannot just add up the 3 vertices, since this is not a "gleichschenkliges" triangle */
    dvec_sub(ico->vertex[thisface->v[1]], ico->vertex[thisface->v[0]], v1);
    dvec_sub(ico->vertex[thisface->v[2]], ico->vertex[thisface->v[0]], v2);
    dcprod(v1, v2, tmp);

    double fact = (diprod(tmp, ico->vertex[thisface->v[0]]) < 0. ? -1. : 1.);
    dsvmul(fact / sqrt(dnorm2(tmp)), tmp, thisface->normal);

    double minSprod = 1000;

    /* Store the smallest scalar product between the normal and each of the verices */
    for (int i = 0; i < 3; i++) {
        fact = diprod(ico->vertex[thisface->v[i]], thisface->normal);
        minSprod = (fact < minSprod) ? fact : minSprod;
    }

    thisface->sprodLim = minSprod;

    if (thisface->sprodLim > 1. or thisface->sprodLim < 0.3) {
        gmx_fatal(FARGS, "sprodlim = %g.\n", thisface->sprodLim);
    }

    ico->nface++;
}

static int icosphere_getMiddlePoint(t_icosphere* ico, int a, int b) {
    dvec middle;

    dvec_add(ico->vertex[a], ico->vertex[b], middle);
    dsvmul(0.5, middle, middle);

    /* Get typical square of distance between vertices */
    int nface = round(20 * pow(4., ico->nrec));
    double difflim2 = 4 * M_PI / nface / 10;

    dvec tmp;

    /* First check if we have this one already */
    for (int i = 0; i < ico->nvertex; i++) {
        dvec_sub(ico->vertex[i], middle, tmp);

        if (dnorm2(tmp) < difflim2) {
            return i;
        }
    }

    /* If we don't have this middle point yet, make it */
    return icosphere_add_vertex(ico, middle[XX], middle[YY], middle[ZZ]);
}

static void icosphere_buildIcosahedron(t_icosphere* ico) {
    const double t = (1.0 + sqrt(5.0)) / 2.0;

    icosphere_add_vertex(ico, -1, t, 0);
    icosphere_add_vertex(ico, 1, t, 0);
    icosphere_add_vertex(ico, -1, -t, 0);
    icosphere_add_vertex(ico, 1, -t, 0);

    icosphere_add_vertex(ico, 0, -1, t);
    icosphere_add_vertex(ico, 0, 1, t);
    icosphere_add_vertex(ico, 0, -1, -t);
    icosphere_add_vertex(ico, 0, 1, -t);

    icosphere_add_vertex(ico, t, 0, -1);
    icosphere_add_vertex(ico, t, 0, 1);
    icosphere_add_vertex(ico, -t, 0, -1);
    icosphere_add_vertex(ico, -t, 0, 1);

    // 5 faces around point 0
    icosphere_add_face(ico, 0, 0, 11, 5);
    icosphere_add_face(ico, 0, 0, 5, 1);
    icosphere_add_face(ico, 0, 0, 1, 7);
    icosphere_add_face(ico, 0, 0, 7, 10);
    icosphere_add_face(ico, 0, 0, 10, 11);
    // 5 adjacent faces
    icosphere_add_face(ico, 0, 1, 5, 9);
    icosphere_add_face(ico, 0, 5, 11, 4);
    icosphere_add_face(ico, 0, 11, 10, 2);
    icosphere_add_face(ico, 0, 10, 7, 6);
    icosphere_add_face(ico, 0, 7, 1, 8);
    // 5 faces around point 3
    icosphere_add_face(ico, 0, 3, 9, 4);
    icosphere_add_face(ico, 0, 3, 4, 2);
    icosphere_add_face(ico, 0, 3, 2, 6);
    icosphere_add_face(ico, 0, 3, 6, 8);
    icosphere_add_face(ico, 0, 3, 8, 9);
    // 5 adjacent faces
    icosphere_add_face(ico, 0, 4, 9, 5);
    icosphere_add_face(ico, 0, 2, 4, 11);
    icosphere_add_face(ico, 0, 6, 2, 10);
    icosphere_add_face(ico, 0, 8, 6, 7);
    icosphere_add_face(ico, 0, 9, 8, 1);
}

static t_icosphere* icosphere_init(int nrec) {
    t_icosphere* ico = new t_icosphere;

    ico->nrec = nrec;
    ico->nvertex = 0;
    ico->nface = 0;
    ico->face = nullptr;
    ico->rec_face = nullptr;
    ico->rec_nface = nullptr;
    ico->vertex = nullptr;

    return ico;
}

t_icosphere* icosphere_build(int nrec, gmx_bool bVerbose) {
    t_icosphere* ico = icosphere_init(nrec);

    snew(ico->rec_face, nrec + 1);
    snew(ico->rec_nface, nrec + 1);

    icosphere_buildIcosahedron(ico);

    for (int irec = 1; irec <= nrec; irec++) {
        /* backup current faces and delete ico->face */
        int nfacenow = round(20 * pow(4., irec - 1));

        for (int f = 0; f < nfacenow; f++) {
            int a = icosphere_getMiddlePoint(ico, ico->rec_face[irec - 1][f].v[0], ico->rec_face[irec - 1][f].v[1]);
            int b = icosphere_getMiddlePoint(ico, ico->rec_face[irec - 1][f].v[1], ico->rec_face[irec - 1][f].v[2]);
            int c = icosphere_getMiddlePoint(ico, ico->rec_face[irec - 1][f].v[2], ico->rec_face[irec - 1][f].v[0]);

            /* Create 4 faces in this recusion level (irec) from the previous recursion level (irec-1) */
            icosphere_add_face(ico, irec, ico->rec_face[irec - 1][f].v[0], a, c);
            icosphere_add_face(ico, irec, ico->rec_face[irec - 1][f].v[1], a, b);
            icosphere_add_face(ico, irec, ico->rec_face[irec - 1][f].v[2], b, c);
            icosphere_add_face(ico, irec, a, b, c);
        }
        /* printf("After recusion %d, have %d faces\n", irec, ico->rec_nface[irec]); */
    }

    /* Store the lowest recusion level */
    ico->face = ico->rec_face[nrec];
    ico->nface = ico->rec_nface[nrec];

    if (bVerbose) {
        printf("\nCreated icosphere with %d vertices and %d faces (%d recursions)\n\n", ico->nvertex, ico->nface, nrec);
    }

    return ico;
}

int icosphere_isInFace(t_icosphere* ico, t_icosphere_face* face, const dvec x, double tol, double* deviation) {
    dvec xcross;
    double invDenom, dot00, dot11, dot01, dot02, dot12, u, v;
    dvec v0, v1, v2;

    double normx2 = dnorm2(x);

    if (normx2 == 0.) {
        /* The origin is in any face */
        DEBUG_PRINTF(("isInFace: accepted because |x| = 0\n"));

        return 1;
    }

    if (face->sprodLim < 0.2) {
        gmx_fatal(FARGS, "Invalid sprodlim %g\n", face->sprodLim);
    }

    double normx = sqrt(normx2);
    double x_dot_n = diprod(x, face->normal);

    if (x_dot_n / normx < face->sprodLim - tol) {
        /*The scalar product is too small, meaning that the angle between x and the
          normal is too large -> x is outside */
        DEBUG_PRINTF(("isInFace: rejected due to scalar product = %+22.15e\n", x_dot_n));

        return -1;
    }

    /* Now we can be sure that x is at least quite close to the triangle. */

    /* get xcross where the line along x crosses the triangle */
    double a_dot_n = diprod(face->normal, ico->vertex[face->v[0]]);
    dsvmul(a_dot_n / x_dot_n, x, xcross);

    /** Now check if xcross is within the triangle. For for algorithm, see e.g.: http://www.blackpawn.com/texts/pointinpoly */
    dvec_sub(ico->vertex[face->v[2]], ico->vertex[face->v[0]], v0);
    dvec_sub(ico->vertex[face->v[1]], ico->vertex[face->v[0]], v1);
    dvec_sub(xcross, ico->vertex[face->v[0]], v2);

    dot00 = diprod(v0, v0);
    dot01 = diprod(v0, v1);
    dot02 = diprod(v0, v2);
    dot11 = diprod(v1, v1);
    dot12 = diprod(v1, v2);

    // Compute barycentric coordinates
    invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
    u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    /* First do exact check */
    gmx_bool bInside = ((u >= 0) and (v >= 0) and (u + v < 1));

    if (bInside) {
        DEBUG_PRINTF(("isInFace: accepted\n"));

        return 1;
    }

    /* Now do approximate check */
    bInside = ((u >= -tol) and (v >= -tol) and (u + v < (1 + tol)));

    if (bInside) {
        DEBUG_PRINTF(("isInFace: approximately accepted\n"));

        /* Get the largest deviation to area inside */
        if (deviation) {
            *deviation = ((-u) > (-v)) ? (-u) : (-v);
            *deviation = ((u + v - 1) > *deviation) ? (u + v - 1) : *deviation;
        }

        return 2;
    }

    DEBUG_PRINTF(("isInFace: rejected due to barycentric coordinates v = %+22.15e, u = %+22.15e, u+v = %+22.15e\n", u, v, u + v));

    return -2;
}

void icosphere_thisFace_to_CGO(FILE* fp, t_icosphere* ico, t_icosphere_face* face, double rcyl, double r, double g,
        double b, double fact) {
    dvec x1, x2;

    fact *= 10;

    for (int i = 0; i < 3; i++) {
        copy_dvec(ico->vertex[face->v[i]], x1);
        copy_dvec(ico->vertex[face->v[(i + 1) % 3]], x2);

        /* triangle on face */
        fprintf(fp, "CYLINDER, %+22.15e,%+22.15e,%+22.15e, %+22.15e,%+22.15e,%+22.15e, %g, %g,%g,%g,%g,%g,%g,\n",
                fact * x1[XX], fact * x1[YY], fact * x1[ZZ], fact * x2[XX], fact * x2[YY], fact * x2[ZZ], rcyl, r, g, b, r, g, b);

        /* origin to vertices */
        fprintf(fp, "CYLINDER, %+22.15e,%+22.15e,%+22.15e, %+22.15e,%+22.15e,%+22.15e, %g, %g,%g,%g,%g,%g,%g,\n",
                fact * x1[XX], fact * x1[YY], fact * x1[ZZ], 0., 0., 0., rcyl, r, g, b, 0., .6, 0.);
    }

    /* the normal vector */
    copy_dvec(face->normal, x1);
    fprintf(fp, "CYLINDER, %+22.15e,%+22.15e,%+22.15e, %+22.15e,%+22.15e,%+22.15e, %g, %g,%g,%g,%g,%g,%g,\n",
            fact * x1[XX], fact * x1[YY], fact * x1[ZZ], 0., 0., 0., rcyl, r, g, b, 0., .6, 0.);
}

int icosphere_x2faceID(t_icosphere *ico, const dvec xgiven) {
    int f = -1, f0, fapprox = -1, res[20], ntest;
    t_icosphere_face* facelist;
    gmx_bool bFoundApprox;
    dvec x;
    double smallestDeviation, devtmp;

    /* First normalize x */
    double norm = sqrt(dnorm2(xgiven));
    if (norm > 0) {
        dsvmul(1. / norm, xgiven, x);
    } else {
        copy_dvec(xgiven, x);
    }

    /* Now go throught the recusions levels */
    f = 0;

    for (int irec = 0; irec <= ico->nrec; irec++) {
        facelist = ico->rec_face[irec];

        if (irec == 0) {
            /* In the zeroth recusion level (Ikosahedron) we test all 20 faces */
            ntest = 20;
        } else {
            /* In all lower recuions levels we need to test only 4 faces */
            ntest = 4;
        }

        f0 = f * 4;
        gmx_bool bFound = bFoundApprox = FALSE;
        smallestDeviation = 1e20;
        DEBUG_PRINTF(("x2faceID recusion: irec = %d, ntest = %d -- f0 = %d\n", irec, ntest, f0));

        for (f = f0; f < (f0 + ntest); f++) {
            res[f - f0] = icosphere_isInFace(ico, facelist + f, x, 1e-5, &devtmp);

            if (res[f - f0] == 1) {
                DEBUG_PRINTF(("\tbreaking loop at f = %d (f0 = %d)\n", f, f0));
                bFound = TRUE;

                break;
            } else if (res[f - f0] == 2) {
                if (devtmp < smallestDeviation) {
                    fapprox = f;
                    smallestDeviation = devtmp;
                    bFoundApprox = TRUE;
                }

                DEBUG_PRINTF(("\tNow fapprox = %d (smallest deviation = %g)\n", fapprox, smallestDeviation));
            }
        }

        if (!bFound and !bFoundApprox) {
            printf("Finally fapprox = %d\n", fapprox);

            FILE* fp = gmx_ffopen("error.py", "w");

            fprintf(fp, "from pymol.cgo import *\nfrom pymol import cmd\n");

            fprintf(fp, "obj=[]\n");
            fprintf(fp, "obj.extend([\n");

            fprintf(fp, "CYLINDER, %+22.15e,%+22.15e,%+22.15e %+22.15e,%+22.15e,%+22.15e %g, %g,%g,%g,%g,%g,%g,\n",
                    10 * x[XX], 10 * x[YY], 10 * x[ZZ], 0., 0., 0., 0.1, .1, 1., .2, .1, 1., .1);

            for (int k = f0; k < (f0 + 4); k++) {
                icosphere_thisFace_to_CGO(fp, ico, facelist + k, k == (f0 + 3) ? 0.1 : 0.05, 1. - 0.3 * (k - f0), .1, .3 * (k - f0), 1.);
            }

            fprintf(fp, "])\n");
            /* IMPORTANT: CYLINDER statements should NOT be within a BEGIN/END block !!! */
            fprintf(fp, "cmd.load_cgo(obj,'ico_error')\n");
            gmx_ffclose(fp);

            /* Advanced debugging... */
            fprintf(stderr, "xgiven: %+22.15e %+22.15e %+22.15e \n", xgiven[XX], xgiven[YY], xgiven[ZZ]);

            gmx_fatal(FARGS, "Could not find a face in which is x = %+22.15e %+22.15e %+22.15e in recursion level %d\n"
                    "Rejection keys were %d %d %d %d", x[XX], x[YY], x[ZZ], irec, res[0], res[1], res[2], res[3]);
        } else if (!bFound and bFoundApprox) {
            DEBUG_PRINTF(("Did not find a numerically exact face in rec lvl %d, but %d matches approximately\n",
                    irec, fapprox));

            /* The point was not exacly in one triangle, but nearly in (some) trianle(s) */
            f = fapprox;
        }

        DEBUG_PRINTF(("\tFound  f = %d\n", f));

        if (f < 0 or f >= ico->nface) {
            gmx_fatal(FARGS, "Found invalid face ID %d in recursion %d\n", f, irec);
        }
    }

    return f;
}
