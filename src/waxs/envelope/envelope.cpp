#include <gromacs/math/vec.h>  // dvec

#include <gromacs/utility/smalloc.h>  // snew, srenew, sfree

#include <waxs/envelope/envelope.h>  // gmx_envelope
#include <waxs/envelope/icosphere.h>  // icosphere_build

// TODO: It should be the structure constructor.
gmx_envelope* gmx_envelope_init(int nrec, gmx_bool bVerbose) {
    gmx_envelope* envelope = new gmx_envelope;

    envelope->bVerbose = bVerbose;
    envelope->d = -1;
    envelope->bHaveEnv = FALSE;
    envelope->bOriginInside = FALSE;
    envelope->nDefined = 0;
    envelope->minInner = -1;
    envelope->maxInner = -1;
    envelope->minOuter = -1;
    envelope->maxOuter = -1;
    envelope->vol = -1;
    envelope->ftunit_re = nullptr;
    envelope->ftunit_im = nullptr;
    envelope->bHaveFourierTrans = FALSE;
    envelope->ftdens_re = nullptr;
    envelope->ftdens_im = nullptr;
    envelope->bHaveSolventFT = FALSE;
    envelope->ngrid = 100;
    envelope->solventNelec = nullptr;
    envelope->solventNelecTotal = -1;
    envelope->nSolventStep = 0;
    envelope->surfElemOuter = envelope->surfElemInner = nullptr;
    clear_dvec(envelope->bsphereCent);
    envelope->bsphereR2 = -1;

    /* Make an icosohere mesh */
    envelope->nrec = nrec;
    envelope->ico = icosphere_build(nrec, envelope->bVerbose);

    int nrays = envelope->ico->nvertex;
    envelope->nSurfElems = envelope->ico->nface;
    envelope->nrays = nrays;
    snew(envelope->s, nrays);
    snew(envelope->inner, nrays);

    for (int j = 0; j < nrays; j++) {
        envelope->inner[j] = 1e20;
    }

    snew(envelope->outer, nrays);
    snew(envelope->r, nrays);
    snew(envelope->isDefined, nrays);

    for (int j = 0; j < nrays; j++) {
        copy_dvec(envelope->ico->vertex[j], envelope->r[j]);
        envelope->s[j].theta = acos(envelope->r[j][ZZ]);
        envelope->s[j].phi = atan2(envelope->r[j][YY], envelope->r[j][XX]);
    }

    /* Init solvent density array */
    envelope->nSolventStep = 0;
    snew(envelope->solventNelec, envelope->nSurfElems);

    for (int f = 0; f < envelope->nSurfElems; f++) {
        snew(envelope->solventNelec[f], envelope->ngrid);
    }

    envelope->solventNelecNorm = 0.;

    envelope->grid_density = nullptr;

    return envelope;
}
